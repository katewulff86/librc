# librc
A small, simple library for cycle-detecting reference counting.

## Licence and Copyright
**librc** is released under the LGPL-3.0 or  later licence,
available at [https://spdx.org/licenses/LGPL-3.0-or-later.html](https://spdx.org/licenses/LGPL-3.0-or-later.html).

Copyright 2022 Kate Wulff <katty.wulff@gmail.com>.

## Rationale
Naive reference counting has issues in the presence of cycles. This library
performs cycle detection in a separate thread so the main threads of execution
are not paused. In fact, all "garbage collection" is performed in this
separate thread so as to not cause delays in the rest of the program.

Optional finaliser functions can be supplied when releasing objects. This is
useful, for example, when releasing an object which holds a file handle,
network connection, or other resource.

## Compilation and Installation
**librc** uses the meson build system. To build, run
```
meson build
ninja -C build
```

The supplied test suite can be run with
```
meson test
```

and then the library can be installed with
```
meson install
```

To include with your project, the simplest way is to include **librc** as a
meson subproject. Add the following wrap file under `subprojects`
```
[wrap-git]
url = https://gitlab.com/katewulff86/librc.git
revision = head
```

Then in your meson build file, add
```
librc_dep = dependency('librc', fallback: ['librc', 'librc_dep'])
```

`librc_dep` can then be included with your project's dependencies.

## Usage
When allocating a new object to be reference counted, instead of using
`malloc` or `calloc`, instead call `rc_make`. To increase the reference
count, call `rc_keep`, and to decrement it, call `rc_drop`. 

### Simple example
```c
    #include <rc.h>
    struct my_type {
        rc rc;
        int c;
    };
    
    rc_meta my_type_meta = {
        .rc = {
            .reference_count = 1,
            .meta = &rc_meta_meta_static,
        }
        .size = sizeof(struct my_type),
    };
    
    void do_something(struct my_type *o) {
        rc_keep(o);
        /* something */
        rc_drop(o);
    }
    
    int main() {
        rc_init();
        struct my_type *my_object = rc_make(&my_type_meta).
        do_something(my_object);
        rc_drop(my_object);
        rc_kill();
        
        return 0;
    }
```
Obviously in real world code, the `rc_keep` and `rc_drop` at the start/end of
`do_something` would be dropped as they are redundant.

There are a few things to note. First of all, `struct my_type` requires an
`rc` struct at the begining to be reference counted. `rc_make` takes one
argument, a pointer to a meta structure to describe the object. `rc_make`
automatically sets all bytes in the return object to `NUL`. The meta structure,
`rc_meta` is described below.

The library must be initialised with a call to `rc_init`. This starts the
separate thread which performs cycle detection. During shutdown, call
`rc_kill`. Calling `rc_init` and `rc_kill` multiple times is completely safe.

`rc_keep` takes a pointer to a reference counted object, increments its
reference count, and returns the same pointer.

`rc_drop` takes a pointer to a reference counted object, decrements its
reference counted, and possibly frees it (if that was the last reference, or if
the object is found to be only reachable in its own cycle).

### Meta
The meta structure, `rc_meta`, describes how finalise an object (necessary if
objects holds a resource, such as a file handle or database connection), and
how to trace the object's reference counted children.
```c
typedef void (*rc_kill_func)(void *, void *);
typedef void (*rc_foreach_func)(void *, void *);
typedef void (*rc_children_func)(void *, rc_foreach_func, void *, void *);

struct rc_meta {
    rc rc;
    size_t size;
    size_t alignment;
    rc_kill_func const kill;            /* deinitialise/finalise */
    void *kill_env;
    rc_children_func const children;    /* call the callback for all
                                         * ref-counted children */
    void *children_env;
    bool cannot_cycle:1;
    bool statically_allocated:1;
};
```

`rc_meta`s are also reference counted, and thus also contain an `rc`. In the
common case where the `rc_meta` is statically allocated (that is, not created
at run-time on the heap), the `rc` member can be ignored, as seen in the example
below.

`size` and `alignment` are used by the allocator when creating the object.
`alignment` can be set to zero to use a default alignment.

The `kill` member is a pointer to a function to finalise the object. This is
used to release any resources the object holds. Note that it should *not* be
used to decrement the reference count of the object's members, as this would
interfere with the cycle detection algorithm. Its second argument is an optional
environment pointer. It can be set to `NULL` in the common case it is not used.

The `children` member is a pointer to a function to be called on each of the
object's children. The first argument is the object, the second is the function
to be called on each child, the third is an environment variable to be passed
to this function, and the fourth is the `children` function's own environment,
if it has one.

`cannot_cycle` is a flag which can be set to true if there is no possible way
that the object can lead to a reference cycle. If true, the cycle detector
will not run for this object.

`statically_allocated` should be used for objects which are not dynamically
allocated on the heap. These objects will not be freed, even if their reference
counts drop to zero.

Any unused fields can be ignored, as seen in the example below, where only
`size`, `kill` and `children` are filled. If an object holds no resource and
has no reference counted children, the only required field is `size`.

### List of file handles example
```c
typedef struct list list;
struct list {
    FILE *file;
    list *next;
}

void list_kill(list *, void *);
void list_children(list *, rc_foreach_func, void *, void *);

rc_meta list_meta = {
    .size = sizeof(list),
    .kill = (rc_kill_func) list_kill,
    .children = (rc_children_func) list_children,
};

void list_kill(list *lst, void *env) {
    (void) env;
    fclose(lst->file);
}

void list_children(list *lst, rc_foreach_func func, void *func_env, void *env) {
    (void) env;
    func(lst->next, env);
}

list *add_to_list(FILE *file, list *rest) {
    list *head = rc_make(&list_meta);
    head->file = file;
    head->next = rc_keep(rest);
    
    return head;
}
```

### Full Scheme-style list example
```c
#include <stdio.h>
#include <rc.h>

typedef struct pair pair;

void pair_kill(pair *, void *);
void pair_children(pair *, rc_foreach_func, void *, void *);
size_t length(pair *);

struct pair {
    rc rc;
    void *car;
    void *cdr;
};

static rc_meta pair_meta = {
    .size = sizeof(pair),
    .kill = (rc_kill_func) pair_kill,
    .children = (rc_children_func) pair_children,
};

pair *
cons(void *car, void *cdr)
{
    pair *pair = rc_make(&pair_meta);
    pair->car = rc_keep(car);
    pair->cdr = rc_keep(cdr);

    return pair;
}

void *
car(pair *pair)
{
    return rc_keep(pair->car);
}

void *
cdr(pair *pair)
{
    return rc_keep(pair->cdr);
}

void
set_car(pair *pair, void *car)
{
    rc_drop(pair->car);
    pair->car = rc_keep(car);
}

void
set_cdr(pair *pair, void *cdr)
{
    rc_drop(pair->cdr);
    pair->cdr = rc_keep(cdr);
}

void
pair_kill(pair *pair, void *env)
{
    (void) env;
    printf("pair freed! (%p)\n", (void *) pair);
}

void
pair_children(pair *pair, rc_foreach_func func, void *func_env, void *env)
{
    (void) env;
    func(pair->car, func_env);
    func(pair->cdr, func_env);
}


size_t
length(pair *lst)
{
    if (lst == NULL)
        return 0;

    pair *rest = cdr(lst);
    size_t l = 1 + length(rest);
    rc_drop(rest);

    return l;
}

int
main()
{
    rc_init();

    pair *list = NULL;
    for (int i = 0; i < 10; ++i) {
        pair *temp = cons(NULL, list);
        rc_drop(list);
        list = temp;
    }

    pair *lst = rc_keep(list);
    while (lst != NULL) {
        printf("%lu\n", length(lst));
        pair *tmp = cdr(lst);
        rc_drop(lst);
        lst = tmp;
    }
    rc_drop(lst);
    rc_drop(list);
    rc_kill();

    return 0;
}
```

