#include <stdio.h>
#include <rc.h>

typedef struct pair pair;

void pair_kill(pair *, void *);
void pair_children(pair *, rc_foreach_func, void *, void *);
size_t length(pair *);

struct pair {
    rc rc;
    void *car;
    void *cdr;
};

static rc_meta pair_meta = {
    .size = sizeof(pair),
    .kill = (rc_kill_func) pair_kill,
    .children = (rc_children_func) pair_children,
};

pair *
cons(void *car, void *cdr)
{
    pair *pair = rc_make(&pair_meta);
    pair->car = rc_keep(car);
    pair->cdr = rc_keep(cdr);

    return pair;
}

void *
car(pair *pair)
{
    return rc_keep(pair->car);
}

void *
cdr(pair *pair)
{
    return rc_keep(pair->cdr);
}

void
set_car(pair *pair, void *car)
{
    rc_drop(pair->car);
    pair->car = rc_keep(car);
}

void
set_cdr(pair *pair, void *cdr)
{
    rc_drop(pair->cdr);
    pair->cdr = rc_keep(cdr);
}

void
pair_kill(pair *pair, void *env)
{
    (void) env;
    printf("pair freed! (%p)\n", (void *) pair);
}

void
pair_children(pair *pair, rc_foreach_func func, void *func_env, void *env)
{
    (void) env;
    func(pair->car, func_env);
    func(pair->cdr, func_env);
}


size_t
length(pair *lst)
{
    if (lst == NULL)
        return 0;

    pair *rest = cdr(lst);
    size_t l = 1 + length(rest);
    rc_drop(rest);

    return l;
}

int
main()
{
    rc_init();

    pair *list = NULL;
    for (int i = 0; i < 10; ++i) {
        pair *temp = cons(NULL, list);
        rc_drop(list);
        list = temp;
    }

    pair *lst = rc_keep(list);
    while (lst != NULL) {
        printf("%lu\n", length(lst));
        pair *tmp = cdr(lst);
        rc_drop(lst);
        lst = tmp;
    }
    rc_drop(lst);
    rc_drop(list);
    rc_kill();

    return 0;
}

