#include <stdio.h>
#include <rc.h>
#include <ctest.h>
#include "pair.h"

void *
rc_fixture_setup(void *e)
{
    (void) e;
    rc_init();
    object_count = 0;

    return NULL;
}

void
rc_fixture_teardown(void *e)
{
    (void) e;
    rc_kill();
    assert(object_count == 0);
}

test_fixture rc_fixture = {
    .parent = NULL,
    .setup = rc_fixture_setup,
    .teardown = rc_fixture_teardown
};

static void
smoke_test(void *e)
{
    (void) e;
}

static void
simple_test(void *e)
{
    (void) e;
    pair *pair = pair_make(NULL, NULL);
    rc_drop(pair);
}

static void
list_test(void *e)
{
    (void) e;
    pair *nil = pair_make(NULL, NULL);
    pair *elem1 = pair_make(NULL, nil);
    pair *elem2 = pair_make(NULL, elem1);
    rc_drop(nil);
    rc_drop(elem1);
    rc_drop(elem2);
}

static void
tree_test(void *e)
{
    (void) e;
    pair *nil1 = pair_make(NULL, NULL);
    pair *nil2 = pair_make(NULL, NULL);
    pair *tree = pair_make(nil1, nil2);
    rc_drop(nil1);
    rc_drop(nil2);
    rc_drop(tree);
}

static void
dag_test(void *e)
{
    (void) e;
    pair *nil = pair_make(NULL, NULL);
    pair *dag = pair_make(nil, nil);
    rc_drop(nil);
    rc_drop(dag);
}

static void
simple_cycle_test(void *e)
{
    (void) e;
    pair *pair1 = pair_make(NULL, NULL);
    pair *pair2 = pair_make(NULL, pair1);
    pair_set_cdr(pair1, pair2);
    rc_drop(pair1);
    rc_drop(pair2);
}

static void
tail_cycle_test(void *e)
{
    (void) e;
    pair *nil = pair_make(NULL, NULL);
    pair *pair1 = pair_make(nil, NULL);
    pair *pair2 = pair_make(NULL, pair1);
    pair_set_cdr(pair1, pair2);
    rc_drop(nil);
    rc_drop(pair1);
    rc_drop(pair2);
}

TEST_CASE_WITH_FIXTURE(smoke_test, &rc_fixture)
TEST_CASE_WITH_FIXTURE(simple_test, &rc_fixture)
TEST_CASE_WITH_FIXTURE(list_test, &rc_fixture)
TEST_CASE_WITH_FIXTURE(tree_test, &rc_fixture)
TEST_CASE_WITH_FIXTURE(dag_test, &rc_fixture)
TEST_CASE_WITH_FIXTURE(simple_cycle_test, &rc_fixture)
TEST_CASE_WITH_FIXTURE(tail_cycle_test, &rc_fixture)
TEST_SUITE(librc_test_suite,
           &smoke_test_case,
           &simple_test_case,
           &list_test_case,
           &tree_test_case,
           &dag_test_case,
           &simple_cycle_test_case,
           &tail_cycle_test_case)

TEST_MAIN(&librc_test_suite)

