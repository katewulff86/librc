#include <stdio.h>
#include <rc.h>
#include "pair.h"

size_t object_count = 0;

void
pair_kill(pair *pair, void *env)
{
    (void) pair;
    (void) env;
    --object_count;
}

void
pair_children(pair *pair, rc_foreach_func func, void *func_env, void *env)
{
    (void) env;
    func(pair->car, func_env);
    func(pair->cdr, func_env);
}

static rc_meta pair_meta = {
    .size = sizeof(pair),
    .kill = (rc_kill_func) pair_kill,
    .children = (rc_children_func) pair_children,
};

pair *
pair_make(void *car, void *cdr)
{
    pair *pair = rc_make(&pair_meta);
    pair->car = rc_keep(car);
    pair->cdr = rc_keep(cdr);
    ++object_count;

    return pair;
}

void
pair_set_car(pair *pair, void *car)
{
    rc_drop(pair->car);
    pair->car = rc_keep(car);
}

void
pair_set_cdr(pair *pair, void *cdr)
{
    rc_drop(pair->cdr);
    pair->cdr = rc_keep(cdr);
}

