#ifndef RC_TEST_PAIR_H
#define RC_TEST_PAIR_H

#include <stdlib.h>
#include <rc.h>

typedef struct pair pair;

struct pair {
    rc rc;
    void *car;
    void *cdr;
};

pair           *pair_make(void *, void *);
void            pair_set_car(pair *, void *);
void            pair_set_cdr(pair *, void *);

extern size_t object_count;

#endif
