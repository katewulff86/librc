/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <threads.h>
#include "cycle.h"
#include "rc.h"

typedef struct queue_element queue_element;

struct queue_element {
    rc *object;
    _Atomic (queue_element *) next;
};

static _Atomic (queue_element *) head = NULL;
static _Atomic (queue_element *) tail = NULL;
static _Atomic (queue_element *) free_list = NULL;
static bool collector_started = false;
static volatile bool collector_should_exit = false;
static thrd_t collector_thread;
static cnd_t queue_empty_condition;
static mtx_t queue_empty_mutex;
static atomic_size_t empty_capacity;
static atomic_size_t empty_size;

static mtx_t queue_mutex;

static void     meta_children(rc_meta *, rc_foreach_func, void *, void *);

static queue_element *get_element();
static int      collect(void *);
static void     wait_till_queue_ready();
static queue_element *queue_pop();
static void     return_element_to_free_list(queue_element *element);
static void     drop_lambda(rc *, void *);
static void     check_for_cycle(rc *);
static void     free_objects_in_cycle(rc **, size_t);

rc_meta rc_meta_meta_meta = {
    .rc = {
        .reference_count = 1,
        .meta = &rc_meta_meta_meta,
    },
    .size = sizeof(rc_meta),
    .cannot_cycle = true,
    .statically_allocated = true,
};

rc_meta rc_meta_meta_dynamic = {
    .rc = {
        .reference_count = 1,
        .meta = &rc_meta_meta_meta,
    },
    .size = sizeof(rc_meta),
    .children = (rc_children_func) meta_children,
    .cannot_cycle = false,
    .statically_allocated = false,
};

rc_meta rc_meta_meta_static = {
    .rc = {
        .reference_count = 1,
        .meta = &rc_meta_meta_meta,
    },
    .size = sizeof(rc_meta),
    .children = (rc_children_func) meta_children,
    .cannot_cycle = false,
    .statically_allocated = true,
};


int
rc_init()
{
    if (collector_started) {
        return 0;
    }
    collector_started = true;

    cnd_init(&queue_empty_condition);
    mtx_init(&queue_empty_mutex, mtx_timed);
    mtx_init(&queue_mutex, mtx_plain);
    collector_should_exit = false;
    int res = thrd_create(&collector_thread, collect, NULL);
    if (res != thrd_success) {
        return -1;
    } else {
        return 0;
    }
}

void
rc_kill()
{
    if (collector_started) {
        while (tail != NULL)
            cnd_signal(&queue_empty_condition);
        collector_should_exit = true;
        thrd_join(collector_thread, NULL);
        cnd_destroy(&queue_empty_condition);
        collector_started = false;
    }
}

void
rc_drop(void *object)
{
    if (object == NULL)
        return;

    rc_meta *meta = rc_get_meta(object);
    if (rc_meta_statically_allocated_p(meta))
        return;

    queue_element *element = get_element();
    element->object = object;
    element->next = NULL;
    mtx_lock(&queue_mutex);
    if (tail != NULL) {
        tail->next = element;
    } else {
        head = element;
        cnd_signal(&queue_empty_condition);
    }
    tail = element;
    mtx_unlock(&queue_mutex);
}

void
meta_children(rc_meta *meta, rc_foreach_func func, void *func_env, void *env)
{
    (void) env;
    void *kill_env = rc_meta_get_kill_env(meta);
    void *children_env = rc_meta_get_children_env(meta);

    if (kill_env != NULL)
        func(kill_env, func_env);
    if (children_env != NULL)
        func(children_env, func_env);
}

queue_element *
get_element()
{
    while (true) {
        queue_element *empty_head = free_list;
        if (empty_head == NULL) {
            queue_element *element = malloc(sizeof(*element));
            ++empty_capacity;

            return element;
        } else {
            if (atomic_compare_exchange_weak(&free_list, &empty_head,
                                             empty_head->next)) {
                --empty_size;

                return empty_head;
            }
        }
    }
}

void
rc_final_release(rc *object)
{
    rc_meta *meta = rc_get_meta(object);
    rc_kill_func kill = rc_meta_get_kill(meta);
    void *kill_env = rc_meta_get_kill_env(meta);
    if (kill != NULL)
        kill(object, kill_env);
    rc_foreach(object, (rc_foreach_func) drop_lambda, NULL);
    free(object);
}

void
drop_lambda(rc *object, void *env)
{
    (void) env;

    rc_drop(object);
}

int
collect(void *e)
{
    (void) e;

    while (!collector_should_exit) {
        wait_till_queue_ready();
        while (tail != NULL) {
            queue_element *element = queue_pop();
            rc *object = element->object;
            rc_meta *meta = rc_get_meta(object);

            if (object->reference_count == 1)
                rc_final_release(object);
            else if (rc_meta_cannot_cycle_p(meta))
                object->reference_count -= 1;
            else
                check_for_cycle(object);

            return_element_to_free_list(element);
        }

        thrd_yield();
    }

    return 0;
}

void
wait_till_queue_ready()
{
    struct timespec delay;
    timespec_get(&delay, TIME_UTC);
    delay.tv_sec += 1;
    cnd_timedwait(&queue_empty_condition, &queue_empty_mutex, &delay);
}

queue_element *
queue_pop()
{
    mtx_lock(&queue_mutex);
    queue_element *element = head;
    head = head->next;
    if (head == NULL)
        tail = NULL;
    mtx_unlock(&queue_mutex);

    return element;
}

void
return_element_to_free_list(queue_element *element)
{
    if (empty_size*2 > empty_capacity) {
        free(element);
        empty_capacity--;
    } else {
        while (true) {
            queue_element *empty_head = free_list;
            element->next = empty_head;
            if (atomic_compare_exchange_weak(&free_list, &empty_head,
                                             element)) {
                empty_size++;
                return;
            }
        }
    }
}

void
check_for_cycle(rc *object)
{
    object->reference_count -= 1;
    size_t objects_length;
    rc **objects = rc_objects_in_cycle(object, &objects_length);
    free_objects_in_cycle(objects, objects_length);
    free(objects);
}

void
free_objects_in_cycle(rc **objects, size_t objects_length)
{
    for (size_t i = 0; i < objects_length; ++i) {
        rc *object = objects[i];
        rc_meta const *meta = rc_get_meta(object);
        rc_kill_func kill = rc_meta_get_kill(meta);
        void *kill_env = rc_meta_get_kill_env(meta);
        if (kill != NULL)
            kill(object, kill_env);
        free(object);
    }
}

