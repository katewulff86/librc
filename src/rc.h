/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef RC_H
#define RC_H

#ifndef __EXTERN_C_BEGIN_DECLS
#ifdef __cplusplus
#define __EXTERN_C_BEGIN_DECLS extern "c" {
#define __EXTERN_C_END_DECLS }
#else
#define __EXTERN_C_BEGIN_DECLS
#define __EXTERN_C_END_DECLS
#endif
#endif

#include <stdalign.h>
#include <stdatomic.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdlib.h>
#include <string.h>

__EXTERN_C_BEGIN_DECLS

typedef struct rc rc;
typedef struct rc_meta rc_meta;

typedef void (*rc_kill_func)(void *, void *);
typedef void (*rc_foreach_func)(void *, void *);
typedef void (*rc_children_func)(void *, rc_foreach_func, void *, void *);

struct rc {
    atomic_size_t reference_count;
    rc_meta *meta;
};

struct rc_meta {
    rc rc;
    size_t size;
    size_t alignment;
    rc_kill_func const kill;            /* deinitialise/finalise */
    void *kill_env;
    rc_children_func const children;    /* call the callback for all
                                         * ref-counted children */
    void *children_env;
    bool cannot_cycle:1;
    bool statically_allocated:1;
};

int             rc_init();
void            rc_kill();

static void    *rc_keep(void *);
void            rc_drop(void *);
static void    *rc_make(rc_meta *);
static void     rc_foreach(void *, rc_foreach_func, void *);

static size_t   rc_get_reference_count(rc const *);
static rc_meta *rc_get_meta(rc const *);

static size_t   rc_meta_get_size(rc_meta const *);
static size_t   rc_meta_get_alignment(rc_meta const *);
static rc_kill_func rc_meta_get_kill(rc_meta const *);
static void    *rc_meta_get_kill_env(rc_meta const *);
static rc_children_func rc_meta_get_children(rc_meta const *);
static void    *rc_meta_get_children_env(rc_meta const *);
static bool     rc_meta_cannot_cycle_p(rc_meta const *);
static bool     rc_meta_statically_allocated_p(rc_meta const *);

extern rc_meta rc_meta_meta_static;
extern rc_meta rc_meta_meta_dynamic;

#include <stdio.h>

inline void *
rc_make(rc_meta *meta)
{
    size_t size = rc_meta_get_size(meta);
    rc *rc = calloc(1, size);
    if (rc == NULL)
        return NULL;

    rc->reference_count = 1;
    rc->meta = rc_keep(meta);

    return rc;
}

inline void *
rc_keep(void *o)
{
    if (o == NULL)
        return NULL;

    rc *rc = o;
    rc->reference_count++;

    return o;
}

inline void
rc_foreach(void *o, rc_foreach_func func, void *func_env)
{
    if (o == NULL)
        return;

    rc *object = o;
    rc_meta *meta = rc_get_meta(object);
    rc_children_func children = rc_meta_get_children(meta);
    void *children_env = rc_meta_get_children_env(meta);

    if (children == NULL)
        return;

    children(o, func, func_env, children_env);
}

inline size_t
rc_get_reference_count(rc const *rc)
{
    if (rc == NULL)
        return 0;

    return rc->reference_count;
}

inline rc_meta *
rc_get_meta(rc const *rc)
{
    if (rc == NULL)
        return NULL;

    return rc->meta;
}

inline size_t
rc_meta_get_size(rc_meta const *meta)
{
    if (meta == NULL)
        return 0;

    return meta->size;
}

inline size_t
rc_meta_get_alignment(rc_meta const *meta)
{
    if (meta == NULL)
        return 0;

    return meta->alignment;
}

inline rc_kill_func
rc_meta_get_kill(rc_meta const *meta)
{
    if (meta == NULL)
        return NULL;

    return meta->kill;
}

inline void *
rc_meta_get_kill_env(rc_meta const *meta)
{
    if (meta == NULL)
        return NULL;

    return meta->kill_env;
}

inline rc_children_func
rc_meta_get_children(rc_meta const *meta)
{
    if (meta == NULL)
        return NULL;

    return meta->children;
}

inline void *
rc_meta_get_children_env(rc_meta const *meta)
{
    if (meta == NULL)
        return NULL;

    return meta->children_env;
}

inline bool
rc_meta_cannot_cycle_p(rc_meta const *meta)
{
    if (meta == NULL)
        return false;

    return meta->cannot_cycle;
}

inline bool
rc_meta_statically_allocated_p(rc_meta const *meta)
{
    if (meta == NULL)
        return false;

    return meta->statically_allocated;
}

__EXTERN_C_END_DECLS

#endif

