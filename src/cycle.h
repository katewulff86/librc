/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#ifndef RC_CYCLE_H
#define RC_CYCLE_H

#include <rc.h>

__EXTERN_C_BEGIN_DECLS

rc            **rc_objects_in_cycle(rc *, size_t *);

__EXTERN_C_END_DECLS

#endif

