/*****************************************************
 * Copyright 2022 Kate Wulff <katty.wulff@gmail.com> *
 * SPDX-License-Identifier: LGPL-3.0-or-later        *
 *****************************************************/

#include <stdio.h>
#include <stdint.h>
#include "cycle.h"

typedef struct bucket bucket;
typedef struct table table;

struct bucket {
    rc *object;
    size_t references;
    size_t back_references;
    bool visited;
    bool safe;
};

struct table {
    bucket **buckets;
    size_t buckets_size;
    size_t buckets_capacity;
};

static void     check_for_cycle(table *, bucket *);
static void     check_for_cycle_aux(rc *, void *);
static table   *table_make();
static void     table_free();
static bucket  *table_add(table *, rc *);
static size_t   table_lookup(table const *, rc const *);
static void     table_resize(table *);
static bucket  *bucket_make(rc *);
static void     bucket_free(bucket *);
static uint64_t object_hash(rc const *);
static rc     **objects_in_cycle(table *, size_t *);
static bool     bucket_cycle_p(bucket const *);
static void     mark_safe_objects(table *);

rc **
rc_objects_in_cycle(rc *object, size_t *result_length)
{
    table *table = table_make();
    bucket *bucket = table_add(table, object);
    check_for_cycle(table, bucket);
    mark_safe_objects(table);
    rc **result = objects_in_cycle(table, result_length);
    table_free(table);

    return result;
}

void
check_for_cycle(table *table, bucket *bucket)
{
    if (bucket->visited)
        return;
    bucket->visited = true;
    rc *object = bucket->object;
    rc_foreach(object, (rc_foreach_func) check_for_cycle_aux, table);
}

void
check_for_cycle_aux(rc *object, void *e)
{
    if (object == NULL)
        return;
    table *table = e;
    bucket *bucket = table_add(table, object);
    bucket->back_references += 1;
    check_for_cycle(table, bucket);
}

rc **
objects_in_cycle(table *table, size_t *result_length)
{
    size_t result_capacity = 1;
    rc **result = calloc(sizeof(*result), result_capacity);
    *result_length = 0;

    for (size_t i = 0; i < table->buckets_capacity; ++i) {
        bucket *bucket = table->buckets[i];
        if (bucket_cycle_p(bucket)) {
            result[*result_length] = bucket->object;
            *result_length += 1;
            if (*result_length == result_capacity) {
                result_capacity *= 2;
                result = realloc(result, sizeof(*result) * result_capacity);
            }
        }
    }

    return result;
}

void
mark_children_safe(rc *object, table *table)
{
    if (object == NULL)
        return;
    bucket *bucket = table->buckets[table_lookup(table, object)];
    if (bucket->safe)
        return;
    bucket->safe = true;
    rc_foreach(object, (rc_foreach_func) mark_children_safe, table);
}

void
mark_safe_objects(table *table)
{
    for (size_t i = 0; i < table->buckets_capacity; ++i) {
        bucket *bucket = table->buckets[i];
        if (bucket != NULL && !bucket->safe) {
            if (bucket->back_references < bucket->references) {
                bucket->safe = true;
                rc_foreach(bucket->object, (rc_foreach_func) mark_children_safe,
                           table);
            }
        }
    }
}

bool
bucket_cycle_p(bucket const *bucket)
{
    if (bucket == NULL)
        return false;

    return !bucket->safe;
}

table *
table_make()
{
    table *table = calloc(sizeof(*table), 1);
    table->buckets = calloc(sizeof(*table->buckets), 64);
    table->buckets_size = 0;
    table->buckets_capacity = 64;

    return table;
}

void
table_free(table *table)
{
    for (size_t i = 0; i < table->buckets_capacity; ++i) {
        bucket_free(table->buckets[i]);
    }
    free(table->buckets);
    free(table);
}

bucket *
table_add(table *table, rc *object)
{
    size_t index = table_lookup(table, object);
    bucket *bucket = table->buckets[index];
    if (bucket != NULL)
        return bucket;

    bucket = bucket_make(object);
    table->buckets[index] = bucket;
    table->buckets_size += 1;
    table_resize(table);

    return bucket;
}

bucket *
bucket_make(rc *object)
{
    bucket *bucket = calloc(sizeof(*bucket), 1);
    bucket->object = object;
    bucket->references = object->reference_count;
    bucket->back_references = 0;
    bucket->visited = false;
    bucket->safe = false;

    return bucket;
}

void
bucket_free(bucket *bucket)
{
    free(bucket);
}

size_t
table_lookup(table const *table, rc const *object)
{
    uint64_t hash = object_hash(object);

    uint64_t index = hash % table->buckets_capacity;
    uint64_t skip = 0;
    while (table->buckets[index] != NULL) {
        bucket *bucket = table->buckets[index];
        if (bucket->object == object)
            break;
        ++skip;
        index = (index + skip) % table->buckets_capacity;
    }

    return index;
}

uint64_t
object_hash(rc const *object)
{
    uint64_t hash = (uint64_t) object;
    hash ^= hash << 13;
    hash ^= hash >> 7;
    hash ^= hash << 17;

    return hash;
}

void
table_resize(table *table)
{
    if (table->buckets_size * 4 <= table->buckets_capacity * 3)
        return;

    size_t capacity = table->buckets_capacity;
    bucket **buckets = table->buckets;

    table->buckets_capacity = table->buckets_capacity * 2;
    table->buckets = calloc(sizeof(*table->buckets), table->buckets_capacity);
    for (size_t i = 0; i < capacity; ++i) {
        bucket *bucket = buckets[i];
        if (bucket != NULL) {
            size_t index = table_lookup(table, bucket->object);
            table->buckets[index] = bucket;
        }
    }

    free(buckets);
}

